package jp.co.bulky.fmaintainer.tool.license;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class LicenseGen extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage window) throws Exception {
		try {
			window.getIcons().add(new Image("images/fMLicenseGen.png"));
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(LicenseGen.class.getResource("view/LicenseGen.fxml"));
			TabPane layout = (TabPane) loader.load();

			Scene scene = new Scene(layout);
			window.setScene(scene);
			window.setTitle("fMManager License Generator");

			window.show();
		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}

}
