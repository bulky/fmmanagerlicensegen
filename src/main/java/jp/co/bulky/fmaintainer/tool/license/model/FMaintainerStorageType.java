package jp.co.bulky.fmaintainer.tool.license.model;

public enum FMaintainerStorageType {
	/** 0 : OMSB */
	OMSB("OMSB", "OMSB"),

	/** 1 : Simple Cloud */
	SIMPLE_CLOUD("SimpleCloud", "SimpleCloud"),

	/** 2 : Smart Biz Plus */
	SMART_BIZ("Smart Biz Plus", "SmartBizPlus"),

	/** 3 : その他(WebDav) */
	OTHER("WebDAV", "WebDAV");

	/**
	 * コンストラクタ
	 * @param serviceLabel サービス名ラベル
	 * @param fmaintainerKey fmaintainerキー
	 * @param userIdLabel 認証時ユーザIDラベル
	 * @param passwordLabel 認証時パスワードラベル
	 */
	private FMaintainerStorageType(String serviceLabel, String fmaintainerKey) {
		this.serviceLabel = serviceLabel;
		this.fmaintainerKey = fmaintainerKey;
	};

	/** サービス名ラベル */
	public String serviceLabel;

	/** 企業識別子ラベル */
	public String fmaintainerKey;


	public static FMaintainerStorageType fromKey(String key) {
		for (FMaintainerStorageType type : FMaintainerStorageType.values()) {
			if (type.fmaintainerKey.equals(key)) {
				return type;
			}
		}
		return null;
	}

}
