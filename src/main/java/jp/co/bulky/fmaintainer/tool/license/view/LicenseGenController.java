package jp.co.bulky.fmaintainer.tool.license.view;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.orangesignal.csv.CsvReader;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.StringConverter;
import jp.co.bulky.fmaintainer.common.code.StorageType;
import jp.co.bulky.fmaintainer.secure.FMMLicenseInfoCipher;
import jp.co.bulky.fmaintainer.secure.ServiceInfoCipher;
import jp.co.bulky.fmaintainer.tool.license.model.FMaintainerStorageType;

public class LicenseGenController implements Initializable {

	/** fMManagerカスタマー識別子 */
	@FXML
	private TextField fMManagerCustomerId;

	/** fMManagerファイル共有サービス識別子 */
	@FXML
	private ChoiceBox<StorageType> fMManagerFileShreServiceId;

	/** fMManager有効期限 */
	@FXML
	private DatePicker fMManagerExpirationDate;

	/** fMManagerオプションパック契約情報 */
	@FXML
	private TextField fMManagerAppMaskInfo;

	/** fMManagerライセンスキー */
	@FXML
	private TextField fMManagerLicenseKey;





	/** fMaintainerカスタマー識別子 */
	@FXML
	private TextField fMaintainerCustomerId;

	/** fMaintainer最大ユーザ数 */
	@FXML
	private TextField fMaintainerMaxUsers;

	/** fMaintainer有効期限 */
	@FXML
	private DatePicker fMaintainerExpirationDate;

	/** fMaintainerサービス情報キー */
	@FXML
	private TextField fMaintainerServiceKey;





	/** fMaintainerカスタマー識別子 for 接続基本情報 */
	@FXML
	private TextField fMaintainerCustomerId4Connection;

	/** fMaintainerファイル共有サービス識別子 for 接続基本情報 */
	@FXML
	private ChoiceBox<FMaintainerStorageType> fMaintainerFileShreServiceId4Connection;

	/** fMaintainerサーバ接続基本情報キー */
	@FXML
	private TextField fMaintainerConnectionKey;

	/** fMaintainerサーバ接続基本情報QRコード */
	@FXML
	private ImageView fMaintainerConnectionQR;



	@Override
	public void initialize(URL location, ResourceBundle resources) {

		fMManagerFileShreServiceId.setConverter(new StringConverter<StorageType>() {
			@Override
			public String toString(StorageType paramT) {
				return paramT.serviceLabel;
			}

			@Override
			public StorageType fromString(String paramString) {
				for (StorageType st : StorageType.values()) {
					if (st.serviceLabel.equals(paramString)) {
						return st;
					}
				}
				return null;
			}
		});
		fMManagerFileShreServiceId.getItems().addAll(Arrays.asList(StorageType.values()));
		fMManagerFileShreServiceId.getSelectionModel().select(0);


		String pattern = "yyyy-MM-dd";
		fMManagerExpirationDate.setPromptText(pattern.toLowerCase());
		fMManagerExpirationDate.setConverter(new StringConverter<LocalDate>() {
		     DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

		     @Override
		     public String toString(LocalDate date) {
		         if (date != null) {
		             return dateFormatter.format(date);
		         } else {
		             return "";
		         }
		     }

		     @Override
		     public LocalDate fromString(String string) {
		         if (string != null && !string.isEmpty()) {
		             return LocalDate.parse(string, dateFormatter);
		         } else {
		             return null;
		         }
		     }
		 });

		if (fMManagerExpirationDate.getValue() == null) {
			fMManagerExpirationDate.setValue(LocalDate.now());
		}


		fMaintainerExpirationDate.setPromptText(pattern.toLowerCase());
		fMaintainerExpirationDate.setConverter(new StringConverter<LocalDate>() {
		     DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

		     @Override
		     public String toString(LocalDate date) {
		         if (date != null) {
		             return dateFormatter.format(date);
		         } else {
		             return "";
		         }
		     }

		     @Override
		     public LocalDate fromString(String string) {
		         if (string != null && !string.isEmpty()) {
		             return LocalDate.parse(string, dateFormatter);
		         } else {
		             return null;
		         }
		     }
		 });

		if (fMaintainerExpirationDate.getValue() == null) {
			fMaintainerExpirationDate.setValue(LocalDate.now());
		}



		fMaintainerFileShreServiceId4Connection.setConverter(new StringConverter<FMaintainerStorageType>() {
			@Override
			public String toString(FMaintainerStorageType paramT) {
				return paramT.serviceLabel;
			}

			@Override
			public FMaintainerStorageType fromString(String paramString) {
				for (FMaintainerStorageType st : FMaintainerStorageType.values()) {
					if (st.serviceLabel.equals(paramString)) {
						return st;
					}
				}
				return null;
			}
		});
		fMaintainerFileShreServiceId4Connection.getItems().addAll(Arrays.asList(FMaintainerStorageType.values()));
		fMaintainerFileShreServiceId4Connection.getSelectionModel().select(0);


	}

	/**
	 * (fMManager)テキスト文字列を暗号化した文字列を返却する
	 *
	 * @return 暗号化文字列
	 * @throws IOException
	 */
	@FXML
	private void handleFMManagerEncrypt() {
		if (!fMManagerValid()) {
			return;
		}

		try {
			List<String> values = new LinkedList<String>();
			values.add(fMManagerCustomerId.getText());

			StorageType storageType = fMManagerFileShreServiceId.getSelectionModel().getSelectedItem();
			values.add(storageType == null ? "" : String.valueOf(storageType.ordinal()));

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate date = fMManagerExpirationDate.getValue();
			String dateVal = date == null ? "" : date.format(formatter);

			values.add(dateVal);
			values.add(fMManagerAppMaskInfo.getText());

			String rawKey = String.join(",", values);

			fMManagerLicenseKey.setText(new FMMLicenseInfoCipher(rawKey).encrypt());

		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText(null);
			alert.setContentText("暗号化失敗\n\ndetail: " + e.getMessage());

			alert.showAndWait();
			handleFMManagerKeyClear();
		}

	}

	private boolean fMManagerValid() {
		List<String> messages = new ArrayList<String>();
		if (StringUtils.isEmpty(fMManagerCustomerId.getText())) {
			messages.add("カスタマー識別子が入力されていません。");
		}

		if (StringUtils.isEmpty(fMManagerAppMaskInfo.getText())) {
			messages.add("オプションパック契約情報が入力されていません。");
		} else {
			Pattern pattern = Pattern.compile("^[0-1]+$");
			if(!pattern.matcher(fMManagerAppMaskInfo.getText()).find()) {
				messages.add("オプションパック契約情報が不正です。");
			}
		}

		if (messages.size() > 0) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText(null);
			alert.setContentText(StringUtils.join(messages, "\n"));
			alert.showAndWait();
			return false;
		}
		return true;

	}



	/**
	 * (fMManager)テキスト文字列を復号化した文字列を返却する
	 *
	 * @return 復号化文字列
	 * @throws IOException
	 */
	@FXML
	private void handleFMManagerDecrypt() {

		if (StringUtils.isEmpty(fMManagerLicenseKey.getText())) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText(null);
			alert.setContentText("ライセンスキーが入力されていません。");
			alert.showAndWait();
			return;
		}

		try {
			String rawKey = new FMMLicenseInfoCipher(fMManagerLicenseKey.getText()).decrypt();

			CsvReader reader = new CsvReader(new StringReader(rawKey));
			List<String> values = reader.readValues();
			fMManagerCustomerId.setText(values.get(0));

			try {
				fMManagerFileShreServiceId.getSelectionModel().select(Integer.parseInt(values.get(1)));
			} catch (Exception e) {
				fMManagerFileShreServiceId.getSelectionModel().select(0);
			}

			try {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				LocalDate date = LocalDate.parse(values.get(2), formatter);
				fMManagerExpirationDate.setValue(date);
			} catch (DateTimeParseException e) {
				fMaintainerExpirationDate.setValue(null);
			}
			fMManagerAppMaskInfo.setText(values.get(3));
			reader.close();

		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText(null);
			alert.setContentText("復号化失敗\n\ndetail: " + e.getMessage());

			alert.showAndWait();
			handleFMManagerSorceClear();
		}
	}

	@FXML
	private void handleFMManagerSorceClear() {
		fMManagerCustomerId.setText("");
		fMManagerFileShreServiceId.getSelectionModel().select(0);
		fMManagerExpirationDate.setValue(LocalDate.now());
		fMManagerAppMaskInfo.setText("");
	}

	@FXML
	private void handleFMManagerKeyClear() {
		fMManagerLicenseKey.setText("");
	}


	/**
	 * (fMaintainer)テキスト文字列を暗号化した文字列を返却する
	 *
	 * @return 暗号化文字列
	 * @throws IOException
	 */
	@FXML
	private void handleFMaintainerEncrypt() {
		if (!fMaintainerValid()) {
			return;
		}

		try {
			List<String> values = new LinkedList<String>();
			values.add(fMaintainerCustomerId.getText());
			values.add(fMaintainerMaxUsers.getText());

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate date = fMaintainerExpirationDate.getValue();
			String dateVal = date == null ? "" : date.format(formatter);
			values.add(dateVal);

			String rawKey = String.join(",", values);

			fMaintainerServiceKey.setText(new ServiceInfoCipher(rawKey).encrypt());

		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText(null);
			alert.setContentText("暗号化失敗\n\ndetail: " + e.getMessage());

			alert.showAndWait();
			handleFMaintainerKeyClear();
		}
	}

	private boolean fMaintainerValid() {
		List<String> messages = new ArrayList<String>();
		if (StringUtils.isEmpty(fMaintainerCustomerId.getText())) {
			messages.add("カスタマー識別子が入力されていません。");
		}

		if (StringUtils.isEmpty(fMaintainerMaxUsers.getText())) {
			messages.add("最大ユーザ数が入力されていません。");
		} else if (!NumberUtils.isDigits(fMaintainerMaxUsers.getText())) {
			messages.add("最大ユーザ数が不正です。");
		}

		if (messages.size() > 0) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText(null);
			alert.setContentText(StringUtils.join(messages, "\n"));
			alert.showAndWait();
			return false;
		}
		return true;

	}

	/**
	 * (fMaintainer)テキスト文字列を復号化した文字列を返却する
	 *
	 * @return 復号化文字列
	 * @throws IOException
	 */
	@FXML
	private void handleFMaintainerDecrypt() {

		if (StringUtils.isEmpty(fMaintainerServiceKey.getText())) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText(null);
			alert.setContentText("サービス情報キーが入力されていません。");
			alert.showAndWait();
			return;
		}


		try {
			String rawKey = new ServiceInfoCipher(fMaintainerServiceKey.getText()).decrypt();
			CsvReader reader = new CsvReader(new StringReader(rawKey));
			List<String> values = reader.readValues();
			fMaintainerCustomerId.setText(values.get(0));
			fMaintainerMaxUsers.setText(String.valueOf(NumberUtils.toInt(values.get(1))));

			try {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				LocalDate date = LocalDate.parse(values.get(2), formatter);
				fMaintainerExpirationDate.setValue(date);
			} catch (DateTimeParseException e) {
				fMaintainerExpirationDate.setValue(null);
			}
			reader.close();

		} catch (Exception e) {
			e.printStackTrace();
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText(null);
			alert.setContentText("復号化失敗\n\ndetail: " + e.getMessage());

			alert.showAndWait();
			handleFMaintainerSorceClear();
		}
	}

	@FXML
	private void handleFMaintainerSorceClear() {
		fMaintainerCustomerId.setText("");
		fMaintainerMaxUsers.setText("");
		fMaintainerExpirationDate.setValue(LocalDate.now());
	}

	@FXML
	private void handleFMaintainerKeyClear() {
		fMaintainerServiceKey.setText("");
	}


	/**
	 * (fMaintainer接続情報)テキスト文字列を暗号化した文字列を返却する
	 *
	 * @return 暗号化文字列
	 * @throws IOException
	 */
	@FXML
	private void handleFMaintainerConnectEncrypt() {
		if (!fMaintainerConnectValid()) {
			return;
		}

		try {
			List<String> values = new LinkedList<String>();
			values.add(fMaintainerCustomerId4Connection.getText());
			FMaintainerStorageType storageType = fMaintainerFileShreServiceId4Connection.getSelectionModel().getSelectedItem();
			values.add(storageType == null ? "" : storageType.fmaintainerKey);

			String rawKey = String.join(",", values);

			fMaintainerConnectionKey.setText(new ServiceInfoCipher(rawKey).encrypt());

			Image img = new Image(new ByteArrayInputStream(toByteArray(fMaintainerConnectionKey.getText())));
			fMaintainerConnectionQR.setImage(img);

		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText(null);
			alert.setContentText("暗号化失敗\n\ndetail: " + e.getMessage());

			alert.showAndWait();
			handleFMaintainerConnectKeyClear();
			fMaintainerConnectionQR.setImage(null);
		}
	}

	private byte[] toByteArray(String contents) throws IOException, WriterException {
		BarcodeFormat format = BarcodeFormat.QR_CODE;
		int width = 80;
		int height = 80;

		Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
		hints.put(EncodeHintType.MARGIN, 0);

		try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
			QRCodeWriter writer = new QRCodeWriter();
			BitMatrix bitMatrix = writer.encode(contents, format, width, height, hints);
			MatrixToImageWriter.writeToStream(bitMatrix, "png", output);
			return output.toByteArray();
		}

	}
	private boolean fMaintainerConnectValid() {
		List<String> messages = new ArrayList<String>();
		if (StringUtils.isEmpty(fMaintainerCustomerId4Connection.getText())) {
			messages.add("カスタマー識別子が入力されていません。");
		}

		if (messages.size() > 0) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText(null);
			alert.setContentText(StringUtils.join(messages, "\n"));
			alert.showAndWait();
			return false;
		}
		return true;

	}

	/**
	 * (fMaintainer接続情報)テキスト文字列を復号化した文字列を返却する
	 *
	 * @return 復号化文字列
	 * @throws IOException
	 */
	@FXML
	private void handleFMaintainerConnectDecrypt() {

		if (StringUtils.isEmpty(fMaintainerConnectionKey.getText())) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText(null);
			alert.setContentText("サーバ接続基本情報キーが入力されていません。");
			alert.showAndWait();
			return;
		}


		try {
			String rawKey = new ServiceInfoCipher(fMaintainerConnectionKey.getText()).decrypt();
			CsvReader reader = new CsvReader(new StringReader(rawKey));
			List<String> values = reader.readValues();
			fMaintainerCustomerId4Connection.setText(values.get(0));
			try {
				FMaintainerStorageType storageType = FMaintainerStorageType.fromKey(values.get(1));
				fMaintainerFileShreServiceId4Connection.getSelectionModel().select(storageType.ordinal());
			} catch (Exception e) {
				fMaintainerFileShreServiceId4Connection.getSelectionModel().select(0);
			}

			fMaintainerConnectionKey.setText(new ServiceInfoCipher(rawKey).encrypt());

			Image img = new Image(new ByteArrayInputStream(toByteArray(fMaintainerConnectionKey.getText())));
			fMaintainerConnectionQR.setImage(img);

			reader.close();

		} catch (Exception e) {
			e.printStackTrace();
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText(null);
			alert.setContentText("復号化失敗\n\ndetail: " + e.getMessage());

			alert.showAndWait();
			handleFMaintainerConnectSorceClear();
			fMaintainerConnectionQR.setImage(null);
		}
	}

	@FXML
	private void handleFMaintainerConnectSorceClear() {
		fMaintainerCustomerId4Connection.setText("");
		fMaintainerFileShreServiceId4Connection.getSelectionModel().select(0);
	}

	@FXML
	private void handleFMaintainerConnectKeyClear() {
		fMaintainerConnectionKey.setText("");
		fMaintainerConnectionQR.setImage(null);
	}
}
